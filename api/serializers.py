from rest_framework import serializers
from .models import pertanian
from .models import tanaman


class pertanianSerializer(serializers.ModelSerializer):
    class Meta:
        model = pertanian
        fields = ["id", "bibittanaman",]

class tanamanSerializer(serializers.ModelSerializer):
    class Meta:
        model = tanaman
        fields = ["id", "pupuk","jenispupuk",]