from django.urls import path
from . import views

urlpatterns = [
     path("pertanian/", views.pertanianListCreate.as_view(), name="pertanian-view-create"),
     path("tanaman/", views.tanamanListCreate.as_view(), name="tanaman-view-create")
]
   
