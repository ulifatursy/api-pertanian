from django.shortcuts import render
from rest_framework import generics
from.models import pertanian
from.models import tanaman
from.serializers import pertanianSerializer
from.serializers import tanamanSerializer


class pertanianListCreate(generics.ListCreateAPIView):
    queryset = pertanian.objects.all()
    serializer_class = pertanianSerializer

class tanamanListCreate(generics.ListCreateAPIView):
    queryset = tanaman.objects.all()
    serializer_class =tanamanSerializer